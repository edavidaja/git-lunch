# git lunch

configure git during lunch

refer to [Happy Git With R](https://happygitwithr.com/) in case of emergency.

lines that start with `$` indicate things that you will be entering into a terminal. You don't have to type `$`.

You can use `SHIFT + INS` to paste text into the terminal.

## Create a Gitlab.com account

Nag: use a password manager!

[1password](https://1password.com/)

[dashlane](https://www.dashlane.com/)

[lastpass](https://www.lastpass.com/)
 

## Open Visual Studio Code

- search for `code` in VDI, it should appear
- hit ``CTRL + ` `` to open the terminal
  - change your default terminal to Git Bash
  - Press `CTRL + SHIFT + P` and type "default shell"

## git config

every command you issue here modifies a file that you can edit later if you need to. You can also just reissue the command if you make a mistake. We're setting some global options, but you can also set per-project options.

### set a user name

This can be your real name or the username you used to create a gitlab account

```
$ git config --global user.name "Who Dis"
```

### set your email address

This needs to match your gitlab email address--just use your GAO email.

```
$ git config --global user.email whodis@gao.gov
```

### security stuff you don't care about but this doesn't work unless you do it

[the gritty details](https://blogs.msdn.microsoft.com/phkelley/2014/01/20/adding-a-corporate-or-self-signed-certificate-authority-to-git-exes-store/)  
[even grittier details](https://github.com/desktop/desktop/issues/2471)

```
$ git config --global http.sslBackend "openssl"
$ git config --global http.sslCAInfo T:/datacommunity/git/ca-bundle.crt
```

in limited testing, this cert bundle will also work for `pip`

check the config settings:

```
$ git config --global --list
```

## some git operations

### edit and revise a file

- create a folder on your `G:` drive:
  - `$ mkdir /g/git-lunch`
- open VS Code in that folder
  - `code /g/git-lunch`
- open a file, save it as `readme.md`

[for more on markdown, check out the markdown guide.](https://www.markdownguide.org/getting-started)

#### V1

readme.md:

```
# git lunch

my name is David Aja and I work in the Center for Design, Methods, and Analysis.
```

```
$ git init
$ git add readme.md
$ git commit -m "initial commit"
```

#### V2

readme.md:

```
# git lunch

my name is David Aja and I work in the Center for Enhanced Analytics.
```

```
$ git add .
$ git commit -m "au revoir, cdma"
```

### sync changes with remote

```
$ git remote add origin https://gitlab.com/edavidaja/git-lunch.git
$ git push -u origin master
```

### one more revision

#### V3

readme.md:

```
# git lunch

my name is David Aja and I work in the Center for Statistics and Data Analysis.
```

```
$ git add .
$ git commit -m "auf wiedersehen, CEA"
$ git push
```

You only need to set the upstream for the first push (of each branch).

### clone

"copy a project from the internet to your computer"

```
$ git clone https://gitlab.com/edavidaja/sas_macros.git
```

## additonal resources:

[Learn Git, by Atlassian](https://www.atlassian.com/git)

[Try Git, by Github](https://try.github.io/)

[workflowr](https://jdblischak.github.io/workflowr/), an R package for turning analyses into websites. [an example workflowr website](https://stephenslab.github.io/wflow-divvy/).

Turn a git repo into [interactive notebooks with Binder](https://mybinder.org/). Get your R projects Binder-ready with [holepunch](https://karthik.github.io/holepunch/).
